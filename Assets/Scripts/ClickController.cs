﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UltimasNS;
using UnityEngine.Networking;
using System;
using System.Reflection;

public class ClickController : MonoBehaviour
{
    // Sample Inventory management class that creates a List of Items ( List<Item> ) required to process a transaction. See Inventory.cs for more details.

    /// <summary>
    /// Add item from inventory to the shopping cart each time the item button is clicked.
    /// </summary>
    /// <param name="itemName">Item name.</param>
    public void onItemClick(string itemName)
    {
        Button qtyButton = GetQtyButtonForClickedItem();
        qtyButton.enabled = true;
        qtyButton.gameObject.SetActive(true);

        Inventory.addItemToShoppingCart(itemName);
        Item item = Inventory.getItemfromShoppingCartByName(itemName);

        Text buttonText = qtyButton.transform.GetChild(0).GetComponent<Text>();
        buttonText.text = "x" + item.qty;
    }

    /// <summary>
    /// Add bundle from inventory to the shopping cart 
    /// </summary>
    /// <param name="bundleName">Bundle name.</param>
    public void onBundleClick(string bundleName)
    {
        Button qtyButton = GetQtyButtonForClickedItem();
        qtyButton.enabled = true;
        qtyButton.gameObject.SetActive(true);

        Inventory.addBundleToShoppingCart(bundleName);
        Bundle bundle = Inventory.getBundleFromShoppingCartByName(bundleName);
        Text buttonText = qtyButton.transform.GetChild(0).GetComponent<Text>();
        buttonText.text = "x" + bundle.bundle_qty;
    }

    /// <summary>
    /// Remove item from the shopping cart each time the (-)remove button is clicked.
    /// </summary>
    /// <param name="itemName">Item name.</param>
    public void onRemoveItemClick(string itemName)
    {
        Inventory.removeItemFromShoppingCart(itemName);
        Item item = Inventory.getItemfromShoppingCartByName(itemName);

        GameObject goQtyButton = EventSystem.current.currentSelectedGameObject;
        Text buttonText = goQtyButton.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>();

        if (null == item) goQtyButton.SetActive(false);
        else buttonText.text = "x" + item.qty;
    }

    /// <summary>
    /// Remove item from the shopping cart each time the (-)remove button is clicked.
    /// </summary>
    /// <param name="itemName">Item name.</param>
    public void onRemoveBundleClick(string itemName)
    {
        Inventory.removeBundleFromShoppingCart(itemName);
        Bundle bundle = Inventory.getBundleFromShoppingCartByName(itemName);

        GameObject goQtyButton = EventSystem.current.currentSelectedGameObject;
        Text buttonText = goQtyButton.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>();

        if (null == bundle) goQtyButton.SetActive(false);
        else buttonText.text = "x" + bundle.bundle_qty;
    }

    /// <summary>
    /// Returns the qty button object for clicked item.
    /// </summary>
    /// <returns>The qty button object</returns>
    public Button GetQtyButtonForClickedItem()
    {
        GameObject component = (GameObject)EventSystem.current.currentSelectedGameObject;
        Transform trans = GameObject.Find(component.name).transform;
        Button qtyButton = trans.GetChild(1).GetComponent<Button>();
        return qtyButton;
    }

    /// <summary>
    /// Stub. Invisible button (fogo logo) click.
    /// </summary>
    public void onFogoClick()
    {
        //ON FOGO FC Badge click, bottom right
    }

    /// <summary>
    /// On exit button click.
    /// </summary>
    public void onExitClick()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
