﻿
using System.Collections.Generic;
using UltimasNS;

/// <summary>
/// Simple Inventory class that demonstrates how to set Item properties required for a successful transaction.
/// </summary>
public static class Inventory 
{
    // Create a List to save shopping cart content
    private static List<Item> cart = new List<Item>();

    // Create a List to save Bundles for checkout (shopping cart)
    private static List<Bundle> bundles = new List<Bundle>();


    /// <summary>
    /// Returns the cart content
    /// </summary>
    /// <returns>Shopping cart list</returns>
    public static List<Item> GetCart() 
    {
        return cart;
    }

    /// <summary>
    /// Returns the bundle list content
    /// </summary>
    /// <returns>Bundle list</returns>
    public static List<Bundle> getBundleList() 
    {
        return bundles;
    }

    /// <summary>
    /// Adds the bundle to the shopping cart bundle list and return the updated cart content.
    /// </summary>
    /// <returns>Updated shopping cart bundle list.</returns>
    /// <param name="bundleName">Bundle name (unique key).</param>
    public static List<Bundle> addBundleToShoppingCart(string bundleName)
    {
        Bundle newBundle = GetBundleByName(bundleName);
        // Check items already in cart and if matching item is already present, increment quantity instead of adding the item again
        foreach (Bundle bundle in bundles) 
        {

            if (bundle.bundleid == newBundle.bundleid)
            {
                bundle.bundle_qty = ( 1 + int.Parse( bundle.bundle_qty) )+"";
                AddBundleItemsToShoppingCart(bundleName);
                // since item was found and quantity was incremented, return the updated cart content now
                return (List<Bundle>) bundles;
            }

        }
        //if no match was found, add the item to the cart and return the updated cart content 
        bundles.Add(GetBundleByName(bundleName));
        AddBundleItemsToShoppingCart(bundleName);
        return (List<Bundle>)bundles;
    }

    /// <summary>
    /// Adds the item to the shopping cart list and return the updated cart content.
    /// </summary>
    /// <returns>Updated shopping cart.</returns>
    /// <param name="itemName">Item name (unique key).</param>
    public static List<Item> addItemToShoppingCart(string itemName)
    { 
        // Check items already in cart and if matching item is already present, increment quantity instead of adding the item again
        foreach (Item item in cart)
        {
            if (item.itemid == getItemId(itemName))
            {
                if ("eq_bundle_time" == itemName)
                {
                    item.qty += 50;
                    item.amountInCents += 50*getItemPrice(itemName);
                }
                else
                {
                    item.qty += 1;
                    item.amountInCents += getItemPrice(itemName);
                }

                // since item was found and quantity was incremented, return the updated cart content now
                return (List<Item>)cart; 
            }
        }
        //if no match was found, add the item to the cart and return the updated cart content 
        cart.Add(GetItemByName(itemName));
        return (List<Item>)cart;
    }

    /// <summary>
    /// Removes the item from the shopping cart list and return the updated cart content.
    /// </summary>
    /// <returns>Updated shopping cart.</returns>
    /// <param name="itemName">Item name (unique key).</param>
    public static List<Item> removeItemFromShoppingCart(string itemName)
    {
        // Check items already in cart and if matching item is already present, increment quantity instead of adding the item again
        foreach (Item item in cart)
        {
            if (item.itemid == getItemId(itemName) && item.qty>1)
            {
                if ("eq_bundle_time" == itemName)
                {
                    item.qty -= 50;
                    item.amountInCents -= 50*getItemPrice(itemName);
                    if (item.qty < 1) { cart.Remove(getItemfromShoppingCartByName(itemName)); }
                }
                else
                {
                    item.qty -= 1;
                    item.amountInCents -= getItemPrice(itemName);
                }

                // since item was found and quantity was incremented, return the updated cart content now
                return (List<Item>)cart;
            }
        }
        //if no match was found, add the item to the cart and return the updated cart content 
        cart.Remove(getItemfromShoppingCartByName(itemName));
        return (List<Item>)cart;
    }
    /// <summary>
    /// Removes the bundle from the bundles list and removes bundle items from the shopping cart.
    /// Return the updated bundles object
    /// </summary>
    /// <returns>Updated shopping cart.</returns>
    /// <param name="bundleName">Bundle name (unique key).</param>
    public static List<Bundle> removeBundleFromShoppingCart(string bundleName)
    {
        // Check items already in cart and if matching item is already present, increment quantity instead of adding the item again
        foreach (Bundle bundle in bundles)
        {
            if ( bundle.bundleid == getBundleId(bundleName)+"" && int.Parse(bundle.bundle_qty)>1 ) 
            {
                bundle.bundle_qty = ""+(int.Parse(bundle.bundle_qty) -1);
                RemoveBundleItemsToShoppingCart(bundleName);

                // since item was found and quantity was incremented, return the updated cart content now
                return (List<Bundle>)bundles;
            }
        }
        //if no match was found, add the item to the cart and return the updated cart content 
        bundles.Remove(getBundleFromShoppingCartByName(bundleName));
        RemoveBundleItemsToShoppingCart(bundleName);
        return (List<Bundle>)bundles;
    }
    /// <summary>
    /// Returns Item from the card if present, or null
    /// </summary>
    /// <returns>Item from the shopping cart or null.</returns>
    /// <param name="itemName">Item name (unique key).</param>
    public static Item getItemfromShoppingCartByName(string itemName)
    {
        foreach (Item item in cart)
        {
            if (item.itemid == getItemId(itemName))
            {
                return item;
            }
        }
        return null;
    }
    /// <summary>
    /// Gets the bundle from shopping cart by name.
    /// </summary>
    /// <returns>The bundle instance from shopping cart</returns>
    /// <param name="bundleName">Bundle name.</param>
    public static Bundle getBundleFromShoppingCartByName(string bundleName)
    {
        foreach (Bundle bundle in bundles)
        {
            if (bundle.bundleid == GetBundleByName(bundleName).bundleid)
            {
                return bundle;
            }
        }
        return null;
    }

    /// <summary>
    /// Gets the Item object by unique name identifier.
    /// </summary>
    /// <returns>The item.</returns>
    /// <param name="itemName">Item name (unique key).</param>
    public static Item GetItemByName(string itemName) {
        Item item = new Item();
        switch ( itemName.ToLower() ) {
            case "time":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "shorts":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "gloves":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "whistle":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "shoes":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "eq_bundle_shorts":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "eq_bundle_gloves":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "eq_bundle_shoes":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "eq_bundle_whistle":
                item.amountInCents = getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 1; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            case "eq_bundle_time":
                item.amountInCents = 50*getItemPrice(itemName); // $2.45 should be set as 245 (NOT 2.45)
                item.description = getItemDescription(itemName); // Item short description
                item.qty = 50; // set initial quantity value to ONE
                item.itemid = getItemId(itemName); // item unique int identifier
                break;
            default:
                break;
        }
        return item;
    }
    /// <summary>
    /// Adds the bundle items to shopping cart.
    /// </summary>
    /// <param name="bundleName">Bundle name.</param>
    public static void AddBundleItemsToShoppingCart(string bundleName)
    {
        if (bundleName == "UniformBundle")
        {
            addItemToShoppingCart("eq_bundle_shorts");
            addItemToShoppingCart("eq_bundle_gloves");
            addItemToShoppingCart("eq_bundle_shoes");
        }
        else
        {
            addItemToShoppingCart("eq_bundle_time");
            addItemToShoppingCart("eq_bundle_whistle");
        }
    }
    /// <summary>
    /// Removes the bundle items from the shopping cart.
    /// </summary>
    /// <param name="bundleName">Bundle name.</param>
    public static void RemoveBundleItemsToShoppingCart(string bundleName)
    {
        if (bundleName == "UniformBundle")
        {
            removeItemFromShoppingCart("eq_bundle_shorts");
            removeItemFromShoppingCart("eq_bundle_gloves");
            removeItemFromShoppingCart("eq_bundle_shoes");
        }
        else
        {
            removeItemFromShoppingCart("eq_bundle_time");
            removeItemFromShoppingCart("eq_bundle_whistle");
        }
    }
    /// <summary>
    /// Returns the bundle by name.
    /// </summary>
    /// <returns>The bundle</returns>
    /// <param name="bundleName">Bundle name.</param>
    public static Bundle GetBundleByName(string bundleName) {
        int bundleID = 9;
        string bundleDesc = "";
        string bundleCat = "";
        if (bundleName == "UniformBundle")
        {
            bundleID = 99;
            bundleDesc = "Uniform Bundle";
            bundleCat = "Uniform";
        }
        else
        {
            bundleID = 98;
            bundleDesc = "Equipment Bundle";
            bundleCat = "Equipment";
        }
        return new Bundle(bundleID, 1, bundleDesc, bundleCat);
    }
    /// <summary>
    /// Returns the bundle identifier.
    /// </summary>
    /// <returns>The bundle identifier.</returns>
    /// <param name="bundleName">Bundle name.</param>
    public static int getBundleId(string bundleName) 
    {
        if (bundleName == "UniformBundle")
        {
            return 99;
        }
        else
        {
            return 98;
        }
    }

    /// <summary>
    /// Returns the item unique identifier.
    /// </summary>
    /// <returns>The item unique identifier.</returns>
    /// <param name="itemName">Item name (unique key).</param>
    public static string getItemId(string itemName)
    {
        switch (itemName.ToLower())
        {
            case "time":
                return "1002501";
            case "shorts":
                return "1002502";
            case "gloves":
                return "1002503";
            case "whistle":
                return "1002504";
            case "shoes":
                return "1002505";
            case "eq_bundle_shorts":
                return "9002502";
            case "eq_bundle_gloves":
                return "9002503";
            case "eq_bundle_shoes":
                return "9002505";
            case "eq_bundle_time":
                return "9002501";
            case "eq_bundle_whistle":
                return "9002504";
            default:
                return "";
        }
    }

    /// <summary>
    /// Returns the item price in cents.
    /// </summary>
    /// <returns>item price in cents ($2.45 = 245 NOT 2.45)</returns>
    /// <param name="itemName">Item name (unique key).</param>
    public static int getItemPrice(string itemName)
    {
        switch (itemName.ToLower())
        {
            case "time":
                return 1;
            case "shorts":
                return 10;
            case "gloves":
                return 25;
            case "whistle":
                return 50;
            case "shoes":
                return 200;
            case "eq_bundle_shorts":
                return 10;
            case "eq_bundle_gloves":
                return 5;
            case "eq_bundle_shoes":
                return 200;
            case "eq_bundle_time":
                return 1;
            case "eq_bundle_whistle":
                return 25;
            default:
                return 0;
        }
    }
    /// <summary>
    /// Returns the item short description.
    /// </summary>
    /// <returns>The item short description.</returns>
    /// <param name="itemName">Item name (unique key).</param>
    public static string getItemDescription(string itemName)
    {
        switch (itemName.ToLower())
        {
            case "time":
                return "1 Minute Extra Time";
            case "shorts":
                return "Level 1 Shorts";
            case "gloves":
                return "Level 1 Gloves";
            case "whistle":
                return "Level 1 Whistle";
            case "shoes":
                return "Level 2 Shoes";
            case "eq_bundle_shorts":
                return "Level 1 Shorts";
            case "eq_bundle_gloves":
                return "Level 1 Gloves";
            case "eq_bundle_shoes":
                return "Level 2 Shoes";
            case "eq_bundle_time":
                return "1 Minute Extra Time";
            case "eq_bundle_whistle":
                return "Level 1 Whistle";
            default:
                return "";
        }
    }

}
