﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Modal : MonoBehaviour
{

    public GameObject InfoPopUpPanel;
    public GameObject YesButton;
    public GameObject NoButton; 

    public Text HeaderText;
    public Text BodyText;
    public Text YesButtonText;
    public Text NoButtonText;


    private Action yesCb = new Action(() => { });
    private Action noCb = new Action(()=> { });

    void Start()
    {
        InfoPopUpPanel.SetActive(false);
        setNoButtonVisibility(false);
        setYesButtonText("OK");
    }

    //void Update () { if (Input.GetKeyDown(KeyCode.I)) { toggle(); } } // For Testing ONLY! Popup toggle with "I" key

    public void toggle() { InfoPopUpPanel.SetActive(!InfoPopUpPanel.activeSelf); }

    public void show ()  { InfoPopUpPanel.SetActive(true); }

    public void hide ()  { InfoPopUpPanel.SetActive(false); }

    public void onYesClick() { hide(); yesCb(); }

    public void onNoClick() { hide(); noCb(); }

    public void config(string headerText, string messageText, string yesButtonText, string noButtonText) {
        setHeader(headerText);
        setMessage(messageText);
        setYesButtonText(yesButtonText);
        setNoButtonText(noButtonText);
    }

    public void setHeader(string text) { HeaderText.text = text; }

    public void setMessage(string text) { BodyText.text = text; }

    public void setYesButtonText(string text) { YesButtonText.text = text; }

    public void setNoButtonText(string text) { NoButtonText.text = text; }

    public void setYesButtonCallback(Action func) { yesCb = func; }
    public void setNoButtonCallback (Action func) { noCb = func; }

    public void setYesButtonVisibility(bool visible) { YesButton.SetActive(visible);  }
    public void setNoButtonVisibility(bool visible) { NoButton.SetActive(visible);  }

}
