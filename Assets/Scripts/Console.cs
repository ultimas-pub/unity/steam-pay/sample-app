﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Console : MonoBehaviour 
{
    public GameObject ConsolePanel;
    public static Text consoleText;

    // Start is called before the first frame update
    void Start()
    {
        ConsolePanel.SetActive(true);
        consoleText = GameObject.Find("ConsoleText").GetComponent<Text>();
        clearConsole();
        ConsolePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            toggleConsole();
        }
    }
    public void toggleConsole() {
        ConsolePanel.SetActive(!ConsolePanel.activeSelf);
    }
    public void closeConsole()
    {
        ConsolePanel.SetActive(false);
    }
    public void openConsole()
    {
        ConsolePanel.SetActive(true);
    }
    public void clearConsole() {
        consoleText.text = "Console:\n";
    }
    public static void Log(string line, bool ignore=false)
    {
        if (!ignore) { Debug.Log(line); }
        if (consoleText)
        {
            consoleText.text += line + "\n";
        }
    }

    public static void Log(Dictionary<string, string> line) {
        Log("Dictionary: { ", true);
        foreach (KeyValuePair<string, string> item in line)
        {
            Log("    "+item.Key + " : " + item.Value, true);
        }
        Log("}", true);
    }
    public static void Log(Object line) {
        Debug.Log(line);
        Log(line.ToString(),true);
    }

}
